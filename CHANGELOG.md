## [1.0.13-true.8](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.7...v1.0.13-true.8) (2021-11-17)


### Bug Fixes

* build ([62735d1](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/62735d104dce48a8281b13561c6d6500cc2cc546))
* build ([7ca8992](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/7ca8992d6390dcbe3aa538ac52aed673d4ab5a18))
* build ([961108d](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/961108d9c0c8a72bec7e6805b6d441a5a85caffe))
* build ([4ee38a2](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/4ee38a2d47d4431d83c230ef9d97668cc0004c46))
* fix build ([d5c5a37](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/d5c5a37dd0d3696098e605d16b4d7fd4819ee8df))
* fix publish ([4569859](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/45698596baf311938fb32c107c09cede5a285fba))

## [1.0.13-true.8](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.7...v1.0.13-true.8) (2021-11-17)


### Bug Fixes

* build ([7ca8992](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/7ca8992d6390dcbe3aa538ac52aed673d4ab5a18))
* build ([961108d](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/961108d9c0c8a72bec7e6805b6d441a5a85caffe))
* build ([4ee38a2](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/4ee38a2d47d4431d83c230ef9d97668cc0004c46))
* fix build ([d5c5a37](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/d5c5a37dd0d3696098e605d16b4d7fd4819ee8df))
* fix publish ([4569859](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/45698596baf311938fb32c107c09cede5a285fba))

## [1.0.13-true.8](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.7...v1.0.13-true.8) (2021-11-17)


### Bug Fixes

* build ([961108d](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/961108d9c0c8a72bec7e6805b6d441a5a85caffe))
* build ([4ee38a2](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/4ee38a2d47d4431d83c230ef9d97668cc0004c46))
* fix build ([d5c5a37](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/d5c5a37dd0d3696098e605d16b4d7fd4819ee8df))
* fix publish ([4569859](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/45698596baf311938fb32c107c09cede5a285fba))

## [1.0.13-true.8](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.7...v1.0.13-true.8) (2021-11-17)


### Bug Fixes

* fix publish ([4569859](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/45698596baf311938fb32c107c09cede5a285fba))

## [1.0.13-true.7](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.6...v1.0.13-true.7) (2021-11-17)


### Bug Fixes

* fix publish ([ee5d95e](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/ee5d95e51028941ddeeb48f2868f4304ff99ef4f))

## [1.0.13-true.4](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.3...v1.0.13-true.4) (2021-11-17)


### Bug Fixes

* move semantic-release publish to root folder again ([46a3765](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/46a3765a50821f4765271f6ddbc9979528cd812c))

## [1.0.13-true.3](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.2...v1.0.13-true.3) (2021-11-17)


### Bug Fixes

* fix npm semantic-release order ([deca1cb](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/deca1cb407b17d9c9d3e5cf095500dd844cec791))

## [1.0.13-true.2](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.13-true.1...v1.0.13-true.2) (2021-11-17)


### Bug Fixes

* adjust alpha branch name ([8339289](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/83392897cf5caea49b99bf18f271faf15b7e45eb))
* adjust alpha version ([af47335](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/af47335c095c14ead32ff6b7bc0237fac76b7200))

## [1.0.13-true.1](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.12...v1.0.13-true.1) (2021-11-17)


### Bug Fixes

* change prepareCmd script ([03835c1](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/03835c18bad282479d484c4a92e5320e28f2d75a))
* consider alpha branch ([c178c4c](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/c178c4c97ec69ba0f239b2da53bf0544cbed2a09))
* consider next and alpha branches ([0890cdd](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/0890cdd8153c81d12ec7f42be1f1061bc5e4d544))
* test ([a3ef9fb](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/a3ef9fb941f5cb6d4ce16cdf8900ef731e1f96d1))

## [1.0.12](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.11...v1.0.12) (2021-11-17)


### Bug Fixes

* remove create cdn .sh and convert to azure shellcd ([bc62400](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/bc62400c96f5cf7035d295133a71ee81a6eb0e07))

## [1.0.11](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.10...v1.0.11) (2021-11-17)


### Bug Fixes

* add push tag after release ([383e87b](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/383e87bed1fb6ac59ca80d0f234d3cbcd1a97dbb))

## [1.0.10](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.9...v1.0.10) (2021-11-17)


### Bug Fixes

* commit version message ([59193cb](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/59193cb4c3762e6653c376369c73ff1359cd253b))

## [1.0.9](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.8...v1.0.9) (2021-11-17)


### Bug Fixes

* publish ([a5161c0](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/a5161c065d13b71383afa62c54e2bd89ec309212))

## [1.0.2](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.1...v1.0.2) (2021-11-16)


### Bug Fixes

* fix semantic-release ([06610cf](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/06610cf35caa16d8e35f1f30caf40746dbc47b87))

## [1.0.1](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/compare/v1.0.0...v1.0.1) (2021-11-16)


### Bug Fixes

* adjust build ([b2db164](https://bitbucket.org/dcg-solutions/linx-digital-nx-icons/commits/b2db16440746ed025052600afd30fc0c7674c576))
