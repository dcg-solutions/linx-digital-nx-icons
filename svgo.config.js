module.exports = {
  plugins: [
    {
      name: 'removeAttrs',
      params: {
        attrs: '(fill|width|height)'
      }
    },
    {
      name: 'addAttributesToSVGElement',
      params: {
        attributes: [
          'width="24px"',
          'height="24px"',
          'viewBox="0 0 24 24"'
        ]
      }
    }
  ]
};
