// rollup.config.js
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import analyze from 'rollup-plugin-analyzer';
import babel from 'rollup-plugin-babel';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import typescript from 'rollup-plugin-typescript2';

export default [
  {
    input: './dist/react/index.tsx',
    output: [
      {
        file: './dist/react/index.js',
        format: 'es',
        sourcemap: false
      }
    ],
    plugins: [
      analyze(),
      babel(),
      commonjs(),
      peerDepsExternal(),
      resolve(),
      typescript({
        useTsconfigDeclarationDir: true
      })
    ]
  }
];
