import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconEyeShow = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        d="M12 5.251C7.969 5.183 3.8 8 1.179 10.885a1.663 1.663 0 000 2.226c2.564 2.824 6.721 5.706 10.82 5.637 4.1.069 8.259-2.813 10.825-5.637.57-.633.57-1.593 0-2.226C20.2 8 16.03 5.183 12 5.251z"
        fillRule="evenodd"
        clipRule="evenodd"
        stroke="#494644"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.75 12a3.75 3.75 0 11-7.5-.002 3.75 3.75 0 017.5.002z"
        fillRule="evenodd"
        clipRule="evenodd"
        stroke="#494644"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

SvgIconEyeShow.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconEyeShow;
