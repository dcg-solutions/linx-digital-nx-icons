import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrowDown1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M5.28 9.75a.964.964 0 01-.76-.39L.203 3.784c-.284-.384-.246-.956.086-1.287.331-.331.835-.301 1.134.067L5.198 7.44c.02.028.05.043.082.043a.105.105 0 00.083-.043l3.775-4.876a.761.761 0 01.776-.313c.29.064.526.3.618.618.092.319.026.669-.174.915L6.042 9.358a.969.969 0 01-.762.392z" />
    </svg>
  );
};

SvgIconArrowDown1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrowDown1;
