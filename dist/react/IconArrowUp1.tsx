import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrowUp1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M5.5 2.406c.28 0 .546.136.731.374l4.151 5.343a.973.973 0 01.168.876c-.089.305-.316.532-.595.593a.733.733 0 01-.746-.3L5.579 4.62c-.02-.026-.049-.041-.08-.041s-.059.015-.079.04L1.79 9.293a.733.733 0 01-.745.3C.766 9.533.539 9.306.45 9a.973.973 0 01.168-.876l4.15-5.342a.932.932 0 01.732-.376z" />
    </svg>
  );
};

SvgIconArrowUp1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrowUp1;
