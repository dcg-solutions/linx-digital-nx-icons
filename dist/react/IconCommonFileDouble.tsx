import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconCommonFileDouble = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.625 4.875a.75.75 0 01.75.75V21.75c0 .414.336.75.75.75h13.5a.75.75 0 010 1.5h-13.5a2.25 2.25 0 01-2.25-2.25V5.625a.75.75 0 01.75-.75z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.354 0H16.28a2.461 2.461 0 011.78.775L21.41 4.3c.461.491.718 1.14.716 1.815v.001l-.75-.002h.75v12.028l-.75.018h.75v-.008a2.55 2.55 0 01-2.48 2.599H7.355a2.55 2.55 0 01-2.48-2.599v.008h.75l-.75-.017v.01V2.598v.01l.75-.018h-.75v.008A2.55 2.55 0 017.355 0zm.033 1.5a1.05 1.05 0 00-1.012 1.074V18.176a1.05 1.05 0 001.012 1.074h12.226a1.05 1.05 0 001.012-1.074V6.111c0-.291-.11-.571-.308-.783l-.001-.001-3.35-3.524a.961.961 0 00-.693-.303H7.387z"
      />
    </svg>
  );
};

SvgIconCommonFileDouble.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconCommonFileDouble;
