import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineUp16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8 14.64a.415.415 0 01-.415-.415V1.776a.415.415 0 11.83 0v12.45c0 .23-.186.416-.415.416z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.104 7.878a.415.415 0 01-.587 0L8 2.361 2.483 7.878a.415.415 0 01-.587-.587l5.81-5.81a.415.415 0 01.587 0l5.81 5.81a.415.415 0 010 .587z"
      />
    </svg>
  );
};

SvgArrowLineUp16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineUp16;
