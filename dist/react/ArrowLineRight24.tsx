import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineRight24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2 12c0-.345.28-.625.625-.625h18.75a.625.625 0 110 1.25H2.625A.625.625 0 012 12z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12.183 21.192a.625.625 0 010-.884L20.491 12l-8.308-8.308a.625.625 0 11.884-.884l8.75 8.75a.625.625 0 010 .884l-8.75 8.75a.625.625 0 01-.884 0z"
      />
    </svg>
  );
};

SvgArrowLineRight24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineRight24;
