import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrowUp = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M23.21 11.765L13.555.8a2.641 2.641 0 00-1.768-.8 2.645 2.645 0 00-1.768.8L.367 11.765a1.413 1.413 0 102.121 1.867l7.45-8.466a.25.25 0 01.437.166v17.255a1.413 1.413 0 102.826 0V5.332a.25.25 0 01.438-.165l7.45 8.465a1.413 1.413 0 102.12-1.867z" />
    </svg>
  );
};

SvgIconArrowUp.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrowUp;
