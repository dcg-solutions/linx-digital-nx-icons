import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesRight16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.963 14.539a.498.498 0 01-.046-.704L10.043 8 4.917 2.165a.498.498 0 11.751-.658l5.415 6.164a.498.498 0 010 .658l-5.415 6.164a.5.5 0 01-.705.046z"
      />
    </svg>
  );
};

SvgArrowChimesRight16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesRight16;
