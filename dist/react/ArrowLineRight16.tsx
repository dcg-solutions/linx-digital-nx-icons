import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineRight16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.36 8.258c0-.229.185-.415.415-.415h12.45a.415.415 0 010 .83H1.776a.415.415 0 01-.415-.415z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.122 14.362a.415.415 0 010-.587l5.517-5.517-5.517-5.517a.415.415 0 01.587-.587l5.81 5.81a.415.415 0 010 .588l-5.81 5.81a.415.415 0 01-.587 0z"
      />
    </svg>
  );
};

SvgArrowLineRight16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineRight16;
