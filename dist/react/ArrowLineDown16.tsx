import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineDown16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8 1.36c.23 0 .415.185.415.415v12.45a.415.415 0 01-.83 0V1.776c0-.23.186-.415.415-.415z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.896 8.122a.415.415 0 01.587 0L8 13.639l5.517-5.517a.415.415 0 01.587.587l-5.81 5.81a.415.415 0 01-.587 0l-5.81-5.81a.415.415 0 010-.587z"
      />
    </svg>
  );
};

SvgArrowLineDown16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineDown16;
