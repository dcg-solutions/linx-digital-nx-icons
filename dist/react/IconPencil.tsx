import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconPencil = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.592 0a4.389 4.389 0 013.126 1.279l.003.002a4.387 4.387 0 01-.04 6.231L8.186 22.01a.75.75 0 01-.344.196L.936 23.976a.75.75 0 01-.912-.913l1.77-6.904a.75.75 0 01.196-.344L16.486 1.318A4.389 4.389 0 0119.592 0zm2.067 2.34a2.889 2.889 0 00-4.103.03l-.006.006L3.195 16.73l-1.404 5.478 5.479-1.405 14.36-14.36a2.887 2.887 0 00.03-4.102z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.075 1.73a.75.75 0 011.061 0l5.134 5.134a.75.75 0 01-1.06 1.06l-5.135-5.133a.75.75 0 010-1.061zM1.99 15.814a.75.75 0 011.06 0l5.14 5.129a.75.75 0 01-1.06 1.062l-5.139-5.13a.75.75 0 010-1.061z"
      />
    </svg>
  );
};

SvgIconPencil.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconPencil;
