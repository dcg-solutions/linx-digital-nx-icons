import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconGiftBox = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.5 11.25a.75.75 0 01.75-.75h19.5a.75.75 0 01.75.75v10.5A2.25 2.25 0 0120.25 24H3.75a2.25 2.25 0 01-2.25-2.25v-10.5zM3 12v9.75c0 .414.336.75.75.75h16.5a.75.75 0 00.75-.75V12H3z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0 8.25A2.25 2.25 0 012.25 6h19.5A2.25 2.25 0 0124 8.25v2.25a1.5 1.5 0 01-1.5 1.5h-21A1.5 1.5 0 010 10.5V8.25zm2.25-.75a.75.75 0 00-.75.75v2.25h21V8.25a.75.75 0 00-.75-.75H2.25z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.5 0a.75.75 0 01.75.75c0 1.387.72 2.693 1.869 3.672C8.269 5.402 9.787 6 11.25 6a.75.75 0 010 1.5c-1.85 0-3.708-.745-5.104-1.937C4.747 4.371 3.75 2.677 3.75.75A.75.75 0 014.5 0z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.75.75A.75.75 0 014.5 0c1.85 0 3.708.745 5.104 1.937C11.003 3.129 12 4.823 12 6.75a.75.75 0 01-1.5 0c0-1.387-.72-2.693-1.869-3.672C7.481 2.098 5.963 1.5 4.5 1.5a.75.75 0 01-.75-.75z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.5 0a.75.75 0 01.75.75c0 1.927-.997 3.621-2.396 4.813C16.458 6.755 14.601 7.5 12.75 7.5a.75.75 0 010-1.5c1.463 0 2.982-.598 4.131-1.578C18.03 3.442 18.75 2.137 18.75.75A.75.75 0 0119.5 0z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.396 1.937C15.792.745 17.649 0 19.5 0a.75.75 0 010 1.5c-1.463 0-2.982.598-4.131 1.578-1.148.98-1.869 2.285-1.869 3.672a.75.75 0 01-1.5 0c0-1.927.997-3.621 2.396-4.813z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9 6.75A.75.75 0 019.75 6h4.5a.75.75 0 01.75.75v16.5a.75.75 0 01-.75.75h-4.5a.75.75 0 01-.75-.75V6.75zm1.5.75v15h3v-15h-3z"
      />
    </svg>
  );
};

SvgIconGiftBox.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconGiftBox;
