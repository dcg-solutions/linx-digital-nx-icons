import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconLove = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.905 4.35A4.923 4.923 0 002.02 5.628l-.671-.336.67.336a4.922 4.922 0 00.923 5.684l.011.012 9.047 9.435 9.048-9.438.01-.011a4.923 4.923 0 00-6.962-6.962l-1.566 1.566a.75.75 0 01-1.06 0L9.905 4.35zm12.747.943l.67-.335a6.423 6.423 0 01-1.196 7.408l-9.584 9.997a.75.75 0 01-1.082 0L1.877 12.37A6.422 6.422 0 01.68 4.958m21.973.335l.67-.335a6.423 6.423 0 00-10.285-1.67L12 4.325l-1.036-1.035A6.423 6.423 0 00.68 4.957"
      />
    </svg>
  );
};

SvgIconLove.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconLove;
