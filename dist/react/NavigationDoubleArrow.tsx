import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgNavigationDoubleArrow = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M15.5 5H11l5 7-5 7h4.5l5-7-5-7zm-7 0H4l5 7-5 7h4.5l5-7-5-7z" />
    </svg>
  );
};

SvgNavigationDoubleArrow.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgNavigationDoubleArrow;
