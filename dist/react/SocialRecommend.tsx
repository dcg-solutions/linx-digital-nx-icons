import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgSocialRecommend = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M12 2a10 10 0 100 20 10 10 0 000-20zm6 9.8a.9.9 0 01-.1.5l-2.1 4.9a1.34 1.34 0 01-1.3.8H9a2 2 0 01-2-2v-5a1.28 1.28 0 01.4-1L12 5l.69.69c.182.19.288.438.3.7v.2L12.41 10H17a1 1 0 011 1v.8z" />
    </svg>
  );
};

SvgSocialRecommend.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgSocialRecommend;
