import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesRight24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.442 21.814a.748.748 0 01-.07-1.056L15.067 12 7.373 3.242a.748.748 0 01.07-1.056.751.751 0 011.057.07l8.127 9.25a.748.748 0 010 .988L8.5 21.744a.751.751 0 01-1.058.07z"
      />
    </svg>
  );
};

SvgArrowChimesRight24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesRight24;
