import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrowLeft1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M2.063 5.76c0-.318.13-.62.357-.83L7.531.22a.82.82 0 011.18.094.917.917 0 01-.062 1.238L4.18 5.67a.122.122 0 00-.038.09c0 .035.014.067.039.09l4.47 4.118c.235.207.344.53.286.847s-.274.574-.566.674a.818.818 0 01-.839-.19l-5.11-4.708a1.132 1.132 0 01-.358-.831z" />
    </svg>
  );
};

SvgIconArrowLeft1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrowLeft1;
