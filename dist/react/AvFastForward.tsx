import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgAvFastForward = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M4 18l8.5-6L4 6v12zm9-12v12l8.5-6L13 6z" />
    </svg>
  );
};

SvgAvFastForward.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgAvFastForward;
