import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineUp24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 22a.625.625 0 01-.625-.625V2.625a.625.625 0 111.25 0v18.75c0 .345-.28.625-.625.625z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.192 11.817a.625.625 0 01-.884 0L12 3.509l-8.308 8.308a.625.625 0 11-.884-.884l8.75-8.75a.625.625 0 01.884 0l8.75 8.75a.625.625 0 010 .884z"
      />
    </svg>
  );
};

SvgArrowLineUp24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineUp24;
