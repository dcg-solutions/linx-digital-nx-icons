import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgCommunicationAppRegistration = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M14 4h-4v4h4V4zM8 16H4v4h4v-4zm0-6H4v4h4v-4zm0-6H4v4h4V4zm6 8.42V10h-4v4h2.42L14 12.42zm6.88-1.13l-1.17-1.17a.41.41 0 00-.58 0l-.88.88L20 12.75l.88-.88a.41.41 0 000-.58zM11 18.25V20h1.75l6.67-6.67-1.75-1.75L11 18.25zM20 4h-4v4h4V4z" />
    </svg>
  );
};

SvgCommunicationAppRegistration.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgCommunicationAppRegistration;
