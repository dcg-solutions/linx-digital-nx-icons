import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconPaymentPaypal = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12.284 4.13a2.42 2.42 0 012.383 3.212A3.75 3.75 0 0111.2 10.11H8.91a.75.75 0 01-.732-.913l1-4.48a.75.75 0 01.732-.587h2.374zm1.666 2.99l-.705-.257A.92.92 0 0012.3 5.63h-1.789l-.665 2.98h1.334a2.25 2.25 0 002.044-1.679l.726.189z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.08 1.568A.75.75 0 015.81.99h8.29c1.407 0 2.93.503 3.996 1.595 1.096 1.12 1.629 2.78 1.168 4.891a7.55 7.55 0 01-7.605 6.014H8.767l-1.35 5.43a.75.75 0 01-.727.57H1.81a.75.75 0 01-.73-.922l4-17zm1.324.922l-3.647 15.5h3.346l1.35-5.43a.75.75 0 01.727-.57h3.512a6.05 6.05 0 006.103-4.822l.002-.008c.368-1.677-.07-2.806-.774-3.527-.733-.75-1.84-1.143-2.923-1.143H6.404z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.312 6.777a.75.75 0 011.021.285c.913 1.618.745 4.083-.37 6.1-1.148 2.073-3.345 3.778-6.622 3.828h-3.037l-1.377 5.434a.75.75 0 01-.727.566H5.32a.75.75 0 01-.736-.893l.24-1.24a.75.75 0 011.472.285l-.067.348h3.387l1.377-5.434a.75.75 0 01.727-.566h3.604c2.69-.042 4.417-1.411 5.326-3.054.941-1.701.964-3.596.377-4.638a.75.75 0 01.285-1.021z"
      />
    </svg>
  );
};

SvgIconPaymentPaypal.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconPaymentPaypal;
