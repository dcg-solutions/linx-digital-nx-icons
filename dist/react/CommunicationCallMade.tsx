import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgCommunicationCallMade = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M9 5v2h6.59L4 18.59 5.41 20 17 8.41V15h2V5H9z" />
    </svg>
  );
};

SvgCommunicationCallMade.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgCommunicationCallMade;
