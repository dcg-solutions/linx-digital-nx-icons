import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgImageWbTwighlight = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M16.954 8.66l2.12-2.12 1.415 1.414-2.13 2.12-1.405-1.414zM17.9 14c-.5-2.85-2.95-5-5.9-5s-5.45 2.15-5.9 5h11.8zM2 16h20v4H2v-4zm9-12h2v3h-2V4zM3.54 7.925L4.954 6.51l2.122 2.122-1.415 1.415L3.54 7.925z" />
    </svg>
  );
};

SvgImageWbTwighlight.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgImageWbTwighlight;
