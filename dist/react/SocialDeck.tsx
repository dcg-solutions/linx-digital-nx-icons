import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgSocialDeck = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M22 9L12 2 2 9h9v13h2V9h9zM4.14 12l-1.96.37.82 4.37V22h2l.02-4H7v4h2v-6H4.9l-.76-4zm14.96 4H15v6h2v-4h1.98l.02 4h2v-5.26l.82-4.37-1.96-.37-.76 4z" />
    </svg>
  );
};

SvgSocialDeck.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgSocialDeck;
