import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgClose16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.05 3.051a7.004 7.004 0 019.902-.003 7.01 7.01 0 010 9.904 7.004 7.004 0 01-9.902-.003 6.998 6.998 0 010-9.898zm7.675 7.664a.584.584 0 00.02-.801L8.93 8.099a.146.146 0 010-.206l1.815-1.815a.584.584 0 10-.825-.826L8.104 7.067a.146.146 0 01-.207 0L6.082 5.252a.584.584 0 00-.823.826l1.816 1.815a.145.145 0 010 .206L5.259 9.914a.584.584 0 000 .826c.23.222.596.222.826 0L7.9 8.924a.147.147 0 01.207 0l1.816 1.816a.584.584 0 00.802-.025z"
      />
    </svg>
  );
};

SvgClose16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgClose16;
