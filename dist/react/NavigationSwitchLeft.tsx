import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgNavigationSwitchLeft = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M8.5 8.62v6.76L5.12 12 8.5 8.62zM10 5l-7 7 7 7V5zm4 0v14l7-7-7-7z" />
    </svg>
  );
};

SvgNavigationSwitchLeft.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgNavigationSwitchLeft;
