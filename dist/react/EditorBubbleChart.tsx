import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgEditorBubbleChart = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M7.2 17.6a3.2 3.2 0 100-6.4 3.2 3.2 0 000 6.4zm7.6 2.4a2 2 0 100-4 2 2 0 000 4zm.4-6.4a4.8 4.8 0 100-9.6 4.8 4.8 0 000 9.6z" />
    </svg>
  );
};

SvgEditorBubbleChart.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgEditorBubbleChart;
