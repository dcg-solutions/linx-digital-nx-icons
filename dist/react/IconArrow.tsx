import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrow = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.657 14.435L.293 8.071a1 1 0 010-1.414L6.657.293A1 1 0 018.07 1.707L3.414 6.364H17v2H3.414l4.657 4.657a1 1 0 11-1.414 1.414z"
      />
    </svg>
  );
};

SvgIconArrow.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrow;
