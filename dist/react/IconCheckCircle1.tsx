import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconCheckCircle1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.294 6.691a.773.773 0 01.127 1.086l-8.082 10.225a1.855 1.855 0 01-3.007-.088l-2.52-3.578a.773.773 0 111.263-.89l2.526 3.584.01.015a.308.308 0 00.502.015l.01-.012 8.085-10.23a.773.773 0 011.086-.127z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.258 12.629C.258 5.796 5.796.258 12.628.258 19.462.258 25 5.796 25 12.628 25 19.462 19.461 25 12.629 25 5.796 25 .258 19.461.258 12.629zm12.37-10.825c-5.977 0-10.824 4.847-10.824 10.825 0 5.978 4.847 10.825 10.825 10.825 5.978 0 10.825-4.847 10.825-10.825 0-5.978-4.847-10.825-10.825-10.825z"
      />
    </svg>
  );
};

SvgIconCheckCircle1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconCheckCircle1;
