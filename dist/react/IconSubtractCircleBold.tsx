import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconSubtractCircleBold = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M23.5 12c0 6.351-5.149 11.5-11.5 11.5C5.652 23.493.507 18.348.5 12 .5 5.649 5.65.5 12 .5S23.5 5.649 23.5 12zm-17-1.5a1 1 0 00-1 1v1a1 1 0 001 1h11a1 1 0 001-1v-1a1 1 0 00-1-1h-11z"
      />
    </svg>
  );
};

SvgIconSubtractCircleBold.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconSubtractCircleBold;
