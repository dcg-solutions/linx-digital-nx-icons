import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgCommunicationReadMore = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M22 7h-9v2h9V7zm0 8h-9v2h9v-2zm0-4h-6v2h6v-2zm-9 1L8 7v4H2v2h6v4l5-5z" />
    </svg>
  );
};

SvgCommunicationReadMore.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgCommunicationReadMore;
