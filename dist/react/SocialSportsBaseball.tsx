import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgSocialSportsBaseball = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M3.81 6.28A9.9 9.9 0 002 12c0 2.13.67 4.1 1.81 5.72C6.23 16.95 8 14.68 8 12c0-2.68-1.77-4.95-4.19-5.72zm16.38 0C17.77 7.05 16 9.32 16 12c0 2.68 1.77 4.95 4.19 5.72A9.9 9.9 0 0022 12a9.9 9.9 0 00-1.81-5.72zM14 12c0-3.28 1.97-6.09 4.79-7.33A9.952 9.952 0 0012 2C9.37 2 6.99 3.02 5.21 4.67 8.03 5.91 10 8.72 10 12s-1.97 6.09-4.79 7.33A9.952 9.952 0 0012 22c2.63 0 5.01-1.02 6.79-2.67A8.002 8.002 0 0114 12z" />
    </svg>
  );
};

SvgSocialSportsBaseball.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgSocialSportsBaseball;
