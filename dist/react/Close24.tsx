import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgClose24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.974 11.973c0-5.523 4.476-10 9.999-10 5.522 0 9.999 4.477 9.999 10 0 5.522-4.477 9.999-10 9.999-5.522 0-9.998-4.477-9.998-10zm9.999-8.5a8.5 8.5 0 100 16.999 8.5 8.5 0 000-16.998z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.202 7.743a.75.75 0 010 1.06l-7.399 7.4a.75.75 0 11-1.06-1.061l7.398-7.4a.75.75 0 011.06 0z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.742 7.743a.75.75 0 011.06 0l7.4 7.399a.75.75 0 01-1.06 1.06l-7.4-7.399a.75.75 0 010-1.06z"
      />
    </svg>
  );
};

SvgClose24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgClose24;
