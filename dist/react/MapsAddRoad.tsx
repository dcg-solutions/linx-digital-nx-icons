import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgMapsAddRoad = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M20 18v-3h-2v3h-3v2h3v3h2v-3h3v-2h-3zm0-14h-2v9h2V4zM6 4H4v16h2V4zm7 0h-2v4h2V4zm0 6h-2v4h2v-4zm0 6h-2v4h2v-4z" />
    </svg>
  );
};

SvgMapsAddRoad.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgMapsAddRoad;
