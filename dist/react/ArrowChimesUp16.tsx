import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesUp16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.539 11.037a.498.498 0 01-.704.046L8 5.957l-5.835 5.126a.498.498 0 11-.658-.751l6.164-5.415a.498.498 0 01.658 0l6.164 5.415a.5.5 0 01.046.705z"
      />
    </svg>
  );
};

SvgArrowChimesUp16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesUp16;
