import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrowDown = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M23.098 10.239a1.413 1.413 0 00-1.994.127l-7.448 8.464a.251.251 0 01-.438-.166V1.412a1.412 1.412 0 00-2.824 0v17.252a.251.251 0 01-.438.166l-7.45-8.464a1.413 1.413 0 10-2.119 1.867l9.652 10.966a2.352 2.352 0 003.534 0l9.652-10.967a1.412 1.412 0 00-.127-1.993z" />
    </svg>
  );
};

SvgIconArrowDown.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrowDown;
