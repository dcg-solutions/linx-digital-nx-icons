import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgMapsTransitEnterexit = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M16 18H6V8h3v4.77L15.98 6 18 8.03 11.15 15H16v3z" />
    </svg>
  );
};

SvgMapsTransitEnterexit.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgMapsTransitEnterexit;
