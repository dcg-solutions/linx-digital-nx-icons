import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgDeviceNetworkCell = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M2 22h20V2L2 22zm18-2h-3V9.83l3-3V20z" />
    </svg>
  );
};

SvgDeviceNetworkCell.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgDeviceNetworkCell;
