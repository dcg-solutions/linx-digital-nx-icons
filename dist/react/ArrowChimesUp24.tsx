import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesUp24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.814 16.558a.748.748 0 01-1.056.07L12 8.933l-8.758 7.693a.748.748 0 01-1.056-.07.751.751 0 01.07-1.057l9.25-8.127a.748.748 0 01.988 0l9.25 8.127c.312.273.343.746.07 1.058z"
      />
    </svg>
  );
};

SvgArrowChimesUp24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesUp24;
