import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgAvControlCamera = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M15.54 5.54L13.77 7.3 12 5.54 10.23 7.3 8.46 5.54 12 2l3.54 3.54zm2.92 10l-1.76-1.77L18.46 12l-1.76-1.77 1.76-1.77L22 12l-3.54 3.54zm-10 2.92l1.77-1.76L12 18.46l1.77-1.76 1.77 1.76L12 22l-3.54-3.54zm-2.92-10l1.76 1.77L5.54 12l1.76 1.77-1.76 1.77L2 12l3.54-3.54zM12 15a3 3 0 100-6 3 3 0 000 6z" />
    </svg>
  );
};

SvgAvControlCamera.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgAvControlCamera;
