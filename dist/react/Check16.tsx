import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgCheck16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M13.56 3.21a1.073 1.073 0 00-1.501.227l-5.092 6.91-3.238-2.59a1.075 1.075 0 00-1.343 1.679l4.11 3.288a1.086 1.086 0 001.537-.202l5.759-7.81a1.074 1.074 0 00-.232-1.503z" />
    </svg>
  );
};

SvgCheck16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgCheck16;
