import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineLeft16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.64 8c0 .23-.185.415-.415.415H1.776a.415.415 0 010-.83h12.45c.23 0 .416.186.416.415z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.878 1.896a.415.415 0 010 .587L2.361 8l5.517 5.517a.415.415 0 11-.587.587l-5.81-5.81a.415.415 0 010-.588l5.81-5.81a.415.415 0 01.587 0z"
      />
    </svg>
  );
};

SvgArrowLineLeft16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineLeft16;
