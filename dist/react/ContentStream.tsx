import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgContentStream = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M20 14a2 2 0 100-4 2 2 0 000 4zM4 14a2 2 0 100-4 2 2 0 000 4zm8 8a2 2 0 100-4 2 2 0 000 4zM10.05 8.59L6.03 4.55h-.01l-.31-.32-1.42 1.41 4.02 4.05.01-.01.31.32 1.42-1.41zm3.893.027l4.405-4.392L19.76 5.64l-4.405 4.393-1.412-1.416zM10.01 15.36l-1.42-1.41-4.03 4.01-.32.33 1.41 1.41 4.03-4.02.33-.32zm9.75 2.94l-3.99-4.01-.36-.35L14 15.35l3.99 4.01.35.35 1.42-1.41zM12 6a2 2 0 100-4 2 2 0 000 4z" />
    </svg>
  );
};

SvgContentStream.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgContentStream;
