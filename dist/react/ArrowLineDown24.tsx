import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineDown24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 5c.258 0 .467.209.467.467v14a.467.467 0 01-.934 0v-14c0-.258.21-.467.467-.467z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.137 12.603a.467.467 0 01.66 0L12 18.807l6.203-6.204a.466.466 0 11.66.66l-6.533 6.534a.467.467 0 01-.66 0l-6.533-6.534a.467.467 0 010-.66z"
      />
    </svg>
  );
};

SvgArrowLineDown24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineDown24;
