import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgActionGavel = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M8.08 5.242L5.25 8.07l14.134 14.15 2.83-2.827L8.08 5.242zM12.314.999L9.486 3.827l5.658 5.656 2.828-2.828L12.314.999zM3.827 9.486L1 12.315l5.657 5.656 2.828-2.828-5.657-5.657zM13 21H1v2h12v-2z" />
    </svg>
  );
};

SvgActionGavel.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgActionGavel;
