import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowLineLeft24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22 12c0 .345-.28.625-.625.625H2.625a.625.625 0 110-1.25h18.75c.345 0 .625.28.625.625z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11.817 2.808a.625.625 0 010 .884L3.509 12l8.308 8.308a.625.625 0 11-.884.884l-8.75-8.75a.625.625 0 010-.884l8.75-8.75a.625.625 0 01.884 0z"
      />
    </svg>
  );
};

SvgArrowLineLeft24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowLineLeft24;
