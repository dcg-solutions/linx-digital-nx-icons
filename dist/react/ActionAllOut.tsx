import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgActionAllOut = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        d="M16.21 4.16l4 4v-4h-4zm4 12l-4 4h4v-4zm-12 4l-4-4v4h4zm-4-12l4-4h-4v4zm12.95-.95c-2.73-2.73-7.17-2.73-9.9 0s-2.73 7.17 0 9.9 7.17 2.73 9.9 0 2.73-7.16 0-9.9zm-1.1 8.8a5.438 5.438 0 01-7.7 0 5.438 5.438 0 010-7.7 5.438 5.438 0 017.7 0 5.438 5.438 0 010 7.7z"
        clipPath="url(#action-all_out_svg__a)"
      />
      <defs>
        <clipPath id="action-all_out_svg__a">
          <path d="M0 0h24v24H0z" />
        </clipPath>
      </defs>
    </svg>
  );
};

SvgActionAllOut.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgActionAllOut;
