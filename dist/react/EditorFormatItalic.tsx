import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgEditorFormatItalic = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4h-8z" />
    </svg>
  );
};

SvgEditorFormatItalic.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgEditorFormatItalic;
