import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconBin = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.5 6a.75.75 0 01.75-.75h13.5a.75.75 0 01.75.75v13.5a2.25 2.25 0 01-2.25 2.25H6.75A2.25 2.25 0 014.5 19.5V6zm1.5.75V19.5c0 .414.336.75.75.75h10.5a.75.75 0 00.75-.75V6.75H6z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.75 9.75a.75.75 0 01.75.75v6a.75.75 0 01-1.5 0v-6a.75.75 0 01.75-.75zM14.25 9.75a.75.75 0 01.75.75v6a.75.75 0 01-1.5 0v-6a.75.75 0 01.75-.75zM1.5 6a.75.75 0 01.75-.75h19.5a.75.75 0 010 1.5H2.25A.75.75 0 011.5 6z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.5 4.5a2.25 2.25 0 012.25-2.25h4.5A2.25 2.25 0 0116.5 4.5V6a.75.75 0 01-.75.75h-7.5A.75.75 0 017.5 6V4.5zm2.25-.75A.75.75 0 009 4.5v.75h6V4.5a.75.75 0 00-.75-.75h-4.5z"
      />
    </svg>
  );
};

SvgIconBin.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconBin;
