import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconCreditCard1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0 5.25A2.25 2.25 0 012.25 3h19.5A2.25 2.25 0 0124 5.25v13.5A2.25 2.25 0 0121.75 21H2.25A2.25 2.25 0 010 18.75V5.25zM1.5 9v9.75c0 .414.336.75.75.75h19.5a.75.75 0 00.75-.75V9h-21zm21-1.5h-21V5.25a.75.75 0 01.75-.75h19.5a.75.75 0 01.75.75V7.5zm-18 5.25a.75.75 0 01.75-.75h8.25a.75.75 0 010 1.5H5.25a.75.75 0 01-.75-.75zm0 3a.75.75 0 01.75-.75h5.25a.75.75 0 010 1.5H5.25a.75.75 0 01-.75-.75z"
      />
    </svg>
  );
};

SvgIconCreditCard1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconCreditCard1;
