import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgActionAccountBalance = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M7 10H4v7h3v-7zm6.5 0h-3v7h3v-7zm8.5 9H2v3h20v-3zm-2-9h-3v7h3v-7zm-8-9L2 6v2h20V6L12 1z" />
    </svg>
  );
};

SvgActionAccountBalance.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgActionAccountBalance;
