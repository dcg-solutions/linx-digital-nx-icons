import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconPin = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.5 8.25a4.5 4.5 0 119 0 4.5 4.5 0 01-9 0zm4.5-3a3 3 0 100 6 3 3 0 000-6z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 1.5a6.75 6.75 0 00-6.75 6.75c0 .706.273 1.77.772 3.08.49 1.286 1.167 2.731 1.907 4.168 1.48 2.875 3.184 5.666 4.07 7.002l-.586.39.587-.39.625.415L12 22.5c.886-1.337 2.59-4.127 4.07-7.002.74-1.437 1.418-2.882 1.908-4.168.499-1.31.772-2.374.772-3.08A6.75 6.75 0 0012 1.5zM3.75 8.25a8.25 8.25 0 0116.5 0c0 .997-.362 2.278-.87 3.614-.518 1.358-1.222 2.857-1.976 4.321-1.507 2.928-3.24 5.766-4.154 7.144a1.5 1.5 0 01-2.5 0c-.915-1.378-2.648-4.217-4.155-7.144-.753-1.464-1.458-2.963-1.975-4.321-.509-1.336-.87-2.617-.87-3.614z"
      />
    </svg>
  );
};

SvgIconPin.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconPin;
