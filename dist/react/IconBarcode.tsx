import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconBarcode = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.75 6.787A2.287 2.287 0 013.037 4.5h17.926a2.287 2.287 0 012.287 2.287v10.426a2.287 2.287 0 01-2.287 2.287H3.037A2.287 2.287 0 01.75 17.213V6.787zM3.037 6a.787.787 0 00-.787.787v10.426c0 .435.352.787.787.787h17.926a.787.787 0 00.787-.787V6.787A.787.787 0 0020.963 6H3.037zM4.5 7.5a.75.75 0 01.75.75v7.5a.75.75 0 01-1.5 0v-7.5a.75.75 0 01.75-.75zm3 0a.75.75 0 01.75.75v4.5a.75.75 0 01-1.5 0v-4.5a.75.75 0 01.75-.75zm3 0a.75.75 0 01.75.75v4.5a.75.75 0 11-1.5 0v-4.5a.75.75 0 01.75-.75zm3 0a.75.75 0 01.75.75v4.5a.75.75 0 11-1.5 0v-4.5a.75.75 0 01.75-.75zm3 0a.75.75 0 01.75.75v4.5a.75.75 0 11-1.5 0v-4.5a.75.75 0 01.75-.75zm3 0a.75.75 0 01.75.75v7.5a.75.75 0 11-1.5 0v-7.5a.75.75 0 01.75-.75zM6.75 15.75A.75.75 0 017.5 15h9a.75.75 0 110 1.5h-9a.75.75 0 01-.75-.75z"
      />
    </svg>
  );
};

SvgIconBarcode.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconBarcode;
