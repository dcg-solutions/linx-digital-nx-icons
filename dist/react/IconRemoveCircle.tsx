import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconRemoveCircle = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M23.5 12c0 6.351-5.149 11.5-11.5 11.5C5.652 23.493.507 18.348.5 12 .5 5.649 5.649.5 12 .5S23.5 5.649 23.5 12zM6.777 7.492a1 1 0 00.273.973l3.359 3.359a.25.25 0 010 .353L7.05 15.536a1 1 0 001.414 1.414l3.359-3.359a.25.25 0 01.353 0l3.359 3.359a1 1 0 101.414-1.414l-3.359-3.359a.25.25 0 010-.353l3.359-3.359a1 1 0 10-1.414-1.414l-3.359 3.359a.25.25 0 01-.353 0L8.464 7.051a1 1 0 00-1.687.441z"
      />
    </svg>
  );
};

SvgIconRemoveCircle.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconRemoveCircle;
