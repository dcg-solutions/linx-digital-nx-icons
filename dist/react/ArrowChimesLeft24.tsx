import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesLeft24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.558 2.186a.748.748 0 01.07 1.056L8.933 12l7.693 8.758a.748.748 0 01-.07 1.056.75.75 0 01-1.057-.07l-8.127-9.25a.748.748 0 010-.988l8.127-9.25a.751.751 0 011.058-.07z"
      />
    </svg>
  );
};

SvgArrowChimesLeft24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesLeft24;
