import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconInformationCircle = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12C23.993 5.376 18.624.007 12 0zm.25 5a1.5 1.5 0 110 3 1.5 1.5 0 010-3zM10.5 18.5h4a1 1 0 100-2h-.75a.25.25 0 01-.25-.25V11.5a2 2 0 00-2-2h-1a1 1 0 000 2h.75a.25.25 0 01.25.25v4.5a.25.25 0 01-.25.25h-.75a1 1 0 000 2z"
      />
    </svg>
  );
};

SvgIconInformationCircle.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconInformationCircle;
