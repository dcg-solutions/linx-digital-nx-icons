import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconLock2 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.5 9.5h-.75V6.75a6.75 6.75 0 10-13.5 0V9.5H4.5a2 2 0 00-2 2V22a2 2 0 002 2h15a2 2 0 002-2V11.5a2 2 0 00-2-2zm-7.5 9a2 2 0 110-4 2 2 0 010 4zm3.75-9a.5.5 0 00.5-.5V6.75a4.25 4.25 0 00-8.5 0V9a.5.5 0 00.5.5h7.5z"
      />
    </svg>
  );
};

SvgIconLock2.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconLock2;
