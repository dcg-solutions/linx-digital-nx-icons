import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgRemove16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M13.222 2.778c.19.19.19.499 0 .69l-9.755 9.754a.488.488 0 11-.69-.69l9.756-9.754c.19-.19.499-.19.69 0z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.778 2.778c.19-.19.499-.19.69 0l9.754 9.755a.488.488 0 11-.69.69L2.779 3.466a.488.488 0 010-.69z"
      />
    </svg>
  );
};

SvgRemove16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgRemove16;
