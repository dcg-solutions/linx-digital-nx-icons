import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconPenWrite = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M14.325 4.37a.75.75 0 011.06 0l3.183 3.183a.75.75 0 010 1.06l-6.364 6.364a.75.75 0 01-.348.197l-4.242 1.061a.75.75 0 01-.91-.909l1.06-4.243.728.182-.53-.53 6.363-6.364zM9.17 11.649l-.707 2.829 2.828-.707 5.687-5.687-2.121-2.121-5.687 5.686zM21.22 3.84a.75.75 0 011.06 0l1.06 1.06a2.25 2.25 0 010 3.183l-3.31 3.31a.75.75 0 11-1.06-1.06l3.31-3.31a.75.75 0 000-1.061l-1.06-1.06a.75.75 0 010-1.061z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18.038.659a2.25 2.25 0 013.182 0l1.06 1.06a2.25 2.25 0 01.001 3.182l-3.713 3.712a.75.75 0 11-1.06-1.06L21.22 3.84a.75.75 0 000-1.06m-1.06-1.062a.75.75 0 00-1.061 0l-3.713 3.712a.75.75 0 01-1.06-1.061L18.038.659M2.25 6a.75.75 0 00-.75.75v15c0 .414.336.75.75.75h15a.75.75 0 00.75-.75v-7.5a.75.75 0 011.5 0v7.5A2.25 2.25 0 0117.25 24h-15A2.25 2.25 0 010 21.75v-15A2.25 2.25 0 012.25 4.5h7.5a.75.75 0 110 1.5h-7.5z"
      />
    </svg>
  );
};

SvgIconPenWrite.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconPenWrite;
