import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconAddCircleBold = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 23.5C5.65 23.5.5 18.351.5 12 .507 5.652 5.652.507 12 .5 18.351.5 23.5 5.649 23.5 12S18.351 23.5 12 23.5zm-5.5-13a1 1 0 00-1 1v1a1 1 0 001 1h3.75a.25.25 0 01.25.25v3.75a1 1 0 001 1h1a1 1 0 001-1v-3.75a.25.25 0 01.25-.25h3.75a1 1 0 001-1v-1a1 1 0 00-1-1h-3.75a.25.25 0 01-.25-.25V6.5a1 1 0 00-1-1h-1a1 1 0 00-1 1v3.75a.25.25 0 01-.25.25H6.5z"
      />
    </svg>
  );
};

SvgIconAddCircleBold.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconAddCircleBold;
