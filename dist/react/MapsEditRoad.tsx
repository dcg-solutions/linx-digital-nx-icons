import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgMapsEditRoad = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M18 4h-2v7.9l2-2V4zM6 4H4v16h2V4zm6 0h-2v4h2V4zm0 6h-2v4h2v-4zm0 6h-2v4h2v-4zm10.56-3.41l-1.15-1.15a1.49 1.49 0 00-2.12 0L14 16.73V20h3.27l5.29-5.29c.59-.59.59-1.54 0-2.12zm-5.98 5.86h-1.03v-1.03L19 13.97 20.03 15l-3.45 3.45z" />
    </svg>
  );
};

SvgMapsEditRoad.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgMapsEditRoad;
