import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgEditorScatterPlot = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M7 17a3 3 0 100-6 3 3 0 000 6zm4-8a3 3 0 100-6 3 3 0 000 6zm5.6 11.6a3 3 0 100-6 3 3 0 000 6z" />
    </svg>
  );
};

SvgEditorScatterPlot.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgEditorScatterPlot;
