import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconEyeHide = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        d="M2.783 21l18.75-18M8.963 19.051A9.986 9.986 0 0012 19.5c4.1.069 8.258-2.813 10.824-5.637.57-.633.57-1.593 0-2.226A20.561 20.561 0 0019.75 8.88m-5.337-2.598A9.549 9.549 0 0012 6c-4.031-.067-8.2 2.752-10.821 5.635a1.663 1.663 0 000 2.226 20.8 20.8 0 002.6 2.4M8.25 12.75A3.749 3.749 0 0112 9m3.75 3.749A3.75 3.75 0 0112 16.5"
        stroke="#494644"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

SvgIconEyeHide.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconEyeHide;
