import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesDown24 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.186 7.442a.748.748 0 011.056-.07L12 15.067l8.758-7.693a.748.748 0 011.056.07.75.75 0 01-.07 1.057l-9.25 8.127a.748.748 0 01-.988 0L2.256 8.5a.751.751 0 01-.07-1.058z"
      />
    </svg>
  );
};

SvgArrowChimesDown24.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesDown24;
