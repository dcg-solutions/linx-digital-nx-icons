import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconSearch = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18.978 16.268l4.46 4.46c.749.75.749 1.964 0 2.714a1.944 1.944 0 01-2.715 0l-4.46-4.458a9.846 9.846 0 112.715-2.716zm-1.218-5.463a6.96 6.96 0 00-6.96-6.96 6.968 6.968 0 00-6.96 6.96 6.96 6.96 0 1013.92 0z"
      />
    </svg>
  );
};

SvgIconSearch.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconSearch;
