import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesDown16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.461 4.963a.498.498 0 01.704-.046L8 10.043l5.835-5.126a.498.498 0 11.658.751l-6.164 5.415a.498.498 0 01-.658 0L1.507 5.668a.5.5 0 01-.046-.705z"
      />
    </svg>
  );
};

SvgArrowChimesDown16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesDown16;
