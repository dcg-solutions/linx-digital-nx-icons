import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconCalendar = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.002 5.25A2.25 2.25 0 012.252 3h19.5a2.25 2.25 0 012.25 2.25v16.5a2.25 2.25 0 01-2.25 2.25h-19.5a2.25 2.25 0 01-2.25-2.25V5.25zm2.25-.75a.75.75 0 00-.75.75v16.5c0 .414.336.75.75.75h19.5a.75.75 0 00.75-.75V5.25a.75.75 0 00-.75-.75h-19.5z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.002 9.75A.75.75 0 01.752 9h22.5a.75.75 0 010 1.5H.752a.75.75 0 01-.75-.75zM6.752 0a.75.75 0 01.75.75V6a.75.75 0 11-1.5 0V.75a.75.75 0 01.75-.75zM17.252 0a.75.75 0 01.75.75V6a.75.75 0 01-1.5 0V.75a.75.75 0 01.75-.75z"
      />
    </svg>
  );
};

SvgIconCalendar.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconCalendar;
