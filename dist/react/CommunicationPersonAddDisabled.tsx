import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgCommunicationPersonAddDisabled = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        d="M15 12a4 4 0 100-8 4 4 0 000 8zm8 8v-2c0-2.3-4.1-3.7-6.9-3.9l6 5.9h.9zm-11.6-5.5C9.2 15.1 7 16.3 7 18v2h9.9l4 4 1.3-1.3-21-20.9L0 3.1l4 4V10H1v2h3v3h2v-3h2.9l2.5 2.5zM6 10v-.9l.9.9H6z"
        clipPath="url(#communication-person_add_disabled_svg__a)"
      />
      <defs>
        <clipPath id="communication-person_add_disabled_svg__a">
          <path d="M0 0h24v24H0z" />
        </clipPath>
      </defs>
    </svg>
  );
};

SvgCommunicationPersonAddDisabled.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgCommunicationPersonAddDisabled;
