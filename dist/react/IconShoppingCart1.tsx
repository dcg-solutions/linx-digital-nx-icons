import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconShoppingCart1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M20.776 2.502l-2.637 15.5a2.25 2.25 0 01-2.218 1.873H6a.75.75 0 010-1.5h9.921a.75.75 0 00.74-.625l2.637-15.5A2.25 2.25 0 0121.516.378h.984a.75.75 0 010 1.5h-.984a.75.75 0 00-.74.624zM7.875 20.625a1.125 1.125 0 100 2.25 1.125 1.125 0 000-2.25zM15.375 20.625a1.125 1.125 0 100 2.25 1.125 1.125 0 000-2.25z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.25 4.871A1.5 1.5 0 00.794 6.735l1.45 5.8a3.75 3.75 0 003.638 2.84h12.071a.75.75 0 000-1.5H5.883A2.25 2.25 0 013.7 12.171l-1.45-5.8h17.234a.75.75 0 000-1.5H2.25z"
      />
    </svg>
  );
};

SvgIconShoppingCart1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconShoppingCart1;
