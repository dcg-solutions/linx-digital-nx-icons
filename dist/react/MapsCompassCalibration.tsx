import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgMapsCompassCalibration = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M12 21a4 4 0 100-8 4 4 0 000 8zm0-10.93c1.95 0 3.72.79 5 2.07l5-5C19.44 4.59 15.9 3 12 3S4.56 4.59 2 7.15l5 5a7.06 7.06 0 015-2.08z" />
    </svg>
  );
};

SvgMapsCompassCalibration.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgMapsCompassCalibration;
