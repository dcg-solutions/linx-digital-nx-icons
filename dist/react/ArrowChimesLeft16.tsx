import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgArrowChimesLeft16 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11.037 1.461a.498.498 0 01.046.704L5.957 8l5.126 5.835a.498.498 0 11-.751.658L4.917 8.329a.498.498 0 010-.658l5.415-6.164a.5.5 0 01.705-.046z"
      />
    </svg>
  );
};

SvgArrowChimesLeft16.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgArrowChimesLeft16;
