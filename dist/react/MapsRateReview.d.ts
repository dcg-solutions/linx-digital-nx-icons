import { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
    viewBox?: string;
    height?: string;
    width?: string;
}
declare const SvgMapsRateReview: {
    (props: IconProps): JSX.Element;
    defaultProps: {
        viewBox: string;
        height: string;
        width: string;
    };
};
export default SvgMapsRateReview;
