import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgNavigationSwitchRight = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path
        d="M15.5 15.38V8.62L18.88 12l-3.38 3.38zM14 19l7-7-7-7v14zm-4 0V5l-7 7 7 7z"
        clipPath="url(#navigation-switch_right_svg__a)"
      />
      <defs>
        <clipPath id="navigation-switch_right_svg__a">
          <path d="M0 0h24v24H0z" />
        </clipPath>
      </defs>
    </svg>
  );
};

SvgNavigationSwitchRight.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgNavigationSwitchRight;
