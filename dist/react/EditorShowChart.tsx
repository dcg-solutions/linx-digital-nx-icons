import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgEditorShowChart = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M3.5 18.49l6-6.01 4 4L22 6.92l-1.41-1.41-7.09 7.97-4-4L2 16.99l1.5 1.5z" />
    </svg>
  );
};

SvgEditorShowChart.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgEditorShowChart;
