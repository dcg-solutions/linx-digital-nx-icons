import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgIconArrowRight1 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M8.938 6c0 .33-.13.646-.358.865L3.47 11.77a.798.798 0 01-1.18-.098.984.984 0 01.062-1.289l4.47-4.289A.13.13 0 006.86 6a.13.13 0 00-.04-.094l-4.469-4.29A.984.984 0 012.29.329.798.798 0 013.47.23l5.11 4.904c.228.219.358.534.359.866z" />
    </svg>
  );
};

SvgIconArrowRight1.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgIconArrowRight1;
