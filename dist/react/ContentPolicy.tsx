import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgContentPolicy = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M21 5l-9-4-9 4v6c0 5.55 3.84 10.74 9 12 2.3-.56 4.33-1.9 5.88-3.71l-3.12-3.12a4.994 4.994 0 01-6.29-.64c-1.95-1.95-1.95-5.12 0-7.07s5.12-1.95 7.07 0a5.006 5.006 0 01.64 6.29l2.9 2.9C20.29 15.69 21 13.38 21 11V5zm-9 10a3 3 0 100-6 3 3 0 000 6z" />
    </svg>
  );
};

SvgContentPolicy.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgContentPolicy;
