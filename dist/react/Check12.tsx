import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgCheck12 = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M9.707 3.14a.716.716 0 00-1.001.151L5.31 7.9 3.153 6.17a.717.717 0 00-.896 1.12l2.74 2.192a.724.724 0 001.025-.135l3.84-5.207a.716.716 0 00-.155-1.001z" />
    </svg>
  );
};

SvgCheck12.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgCheck12;
