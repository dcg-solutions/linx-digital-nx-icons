import React, { SVGProps } from "react";
interface IconProps extends SVGProps<SVGSVGElement> {
  viewBox?: string;
  height?: string;
  width?: string;
}

const SvgMapsCategory = (props: IconProps) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} {...props}>
      <path d="M12 2l-5.5 9h11L12 2zm5.5 20a4.5 4.5 0 100-9 4.5 4.5 0 000 9zM3 13.5h8v8H3v-8z" />
    </svg>
  );
};

SvgMapsCategory.defaultProps = {
  viewBox: "0 0 24 24",
  height: "24px",
  width: "24px",
};
export default SvgMapsCategory;
