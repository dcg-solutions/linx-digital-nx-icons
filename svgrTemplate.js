const defaultTemplate = (api, opts, values) => {

  const {
    template
  } = api;
  const {
    typescript
  } = opts;
  const {
    componentName, jsx
  } = values;

  const plugins = [
    'jsx',
    'tsx'
  ];

  if (typescript) {
    plugins.push('typescript');
  }

  const typeScriptTpl = template.smart({
    plugins
  });

  return typeScriptTpl.ast`
  import React, { SVGProps } from 'react'

  interface IconProps extends SVGProps<SVGSVGElement> {
    /**
     * NX Design System | Icon
     * @description viewBox expects a value for viewBox attribute 
     * @example Usually the value is "0 0 width height"
     */
    viewBox?: string;
    /**
     * NX Design System | Icon
     * @description height expects a value for height attribute
     * @example height="24px"
     */
    height?: string;
    /**
     * NX Design System | Icon
     * @description width expects a value for width attribute
     * @example width="24px"
     */
    width?: string;
  }
  
  const ${componentName} = (props: IconProps) => {
    return ${jsx};
  }

  ${componentName}.defaultProps = {
    viewBox: '0 0 24 24',
    height: '24px',
    width: '24px'
  }
  
  export default ${componentName}
  `;
};

module.exports = defaultTemplate;
