const shell = require('shelljs');
const simpleGit = require('simple-git');
const argv = require('minimist');
const getAllFiles = require('./deepGetFilesOfFolder');

require('dotenv')
  .config();

const git = simpleGit();

(async () => {
  try {
    const commitMessage = argv(process.argv.slice(2))['m'];
    const propertiesFiles = getAllFiles('./dist');

    // git add and commit. Files will be pushed during semantic-release
    await git.add(propertiesFiles);
    await git.commit(commitMessage);

    shell.exit(0);
  } catch (e) {
    console.log('-->> Error while committing icon files.');
    console.log(e);

    shell.exit(1);
  }
})();
