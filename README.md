# NX Icons

Automatically generate SVG Icons for the NX Design System based on the Figma NX Icon Library:

- @linx-digital/nx-design-tokens: [https://bitbucket.org/dcg-solutions/linx-digital-nx-design-tokens/src/main/](https://bitbucket.org/dcg-solutions/linx-digital-nx-design-tokens/src/main/).

- @linx-digital/nx-react-components: [https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/src/master/](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/src/master/).

- NX Icons Figma file [https://www.figma.com/file/UQBd7cHKav0hr9oXYp7opJ/LyneDesignSystemIcons?node-id=0%3A12](https://www.figma.com/file/XQqurGDACM6CZ58CMoMKWI/Linx-%E2%99%A5%EF%B8%8E-Icons-(Copy)?node-id=0%3A1)

## Installation

Install the Icons with the following command:
```bash
npm install --save-dev @linx-digital/nx-icons
```

## Usage

Have a look at the dist folder inside node_modules: `./node_modules/@linx-digital/nx-icons/`. There you will find a folder containing all the icons and a json file which will list all icons with their name, id and the svg contents.

## Development

### Conventional Commits

Please follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification to make sure we can automatically determine the next release version if necessary.

### Env Variables

To test and develop the workflow on your local machine, you need to setup an `.env` file in the root directory of the project with the following content:
```bash
FIGMA_ACCESS_TOKEN=XXX
FIGMA_ICONS_FILE_ID=XXX
NPM_TOKEN=XXX
```

## Deployment

The Azure Devops job to build and deploy the Icons will be triggered as soon as the Figma Team Library file for the Icons got changed and published. In this case, the configured Figma Webhook will fire a request to the Express Server hosted on (Azure) App Services (https://linx-nx-figma-server.azurewebsites.net/). After the Express Server has verified that the request comes from the corresponding Figma file, it will trigger the Travis build via API.

Furthermore, Azure Devops is building as soon as a branch gets merged into the master branch. After successful linting, the Lyne Icons package will be published to npm. You can find the package on npm [here](https://www.npmjs.com/package/@linx-digital/nx-icons).
